# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  # your code goes here
  arr.max - arr.min
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  # your code goes here
  arr == arr.sort
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  # your code goes here
  str.downcase.each_char.count { |x| "aeiou".include?(x)}
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  # your code goes here
  new_str = str.downcase.split("")
  new_str.map do |x|
     if "aeiou".include?(x)
       new_str.delete(x)
     end
  end
  new_str.join("")
end


# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  # your code goes here
  sorted = int.to_s.split("").sort.reverse
  p sorted
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  # your code goes here
  arr = str.downcase.split("")
  arr.each_with_index do |letter, idx|
    if letter == arr[idx+1]
      return true
     else
      return false
    end
  end
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  # your code goes here
  "(" + cndns(arr[0..2]) + ") " + cndns(arr[3..5]) + "-" + cndns(arr[6..9])
end

def cndns(arr)
  arr.join("").to_s
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  # your code goes here
  new_arr = str.split(",")
  new_arr.max.to_i - new_arr.min.to_i
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset=1)
 if offset < 0
   rotate_(arr.reverse, offset * -1).reverse
 else
   rotate_(arr, offset)
 end
end

def rotate_(arr, num)
    front = arr.take(num % arr.length)
    back = arr.drop(num % arr.length)
    arr.pop(num)
    arr.shift(num)
  if num == 1
    return back + front
  else
    return back + arr + front
  end
end






# front = arr.take(offset % arr.length)
# back = arr.drop(offset % arr.length)
# if offset < 0
#   front = arr.drop((offset * -1) % arr.length)
#   back = arr.take((offset * -1) % arr.length)
# else
#   front = arr.take(offset % arr.length)
#   back = arr.drop(offset % arr.length)
# end
# if offset < 0
#   arr.pop(offset * -1)
#   arr.shift(offset * -1)
# else
#   arr.pop(offset)
#   arr.shift(offset)
# end
# if offset == 1
#   return back + front
# else
#   return back + arr + front
# end


#to rotate we are taking an offset, and then push/pulling down the order
#with 'take' and 'drop'

#in order to rotate in reverse, we should pull + push instead..either way
#the offset is the same.

#so..take from the front, (altering the array), subtract from back
#and add to the back..

#if offset is negative, plug in reversed array and then reverse back final product..
